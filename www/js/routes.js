angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('menu.home', {
    url: '/home',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('menu.formSewa', {
    url: '/formsewaavanza',
    views: {
      'side-menu21': {
        templateUrl: 'templates/formSewa.html',
        controller: 'formSewaCtrl'
      }
    }
  })

  .state('menu.formSewa2', {
    url: '/formsewaproyektor',
    views: {
      'side-menu21': {
        templateUrl: 'templates/formSewa2.html',
        controller: 'formSewa2Ctrl'
      }
    }
  })

  .state('menu.mencariVendor', {
    url: '/mencarivendor',
    views: {
      'side-menu21': {
        templateUrl: 'templates/mencariVendor.html',
        controller: 'mencariVendorCtrl'
      }
    }
  })

  .state('menu.vendorAnda', {
    url: '/vendoranda',
    views: {
      'side-menu21': {
        templateUrl: 'templates/vendorAnda.html',
        controller: 'vendorAndaCtrl'
      }
    }
  })

  .state('menu.kontakVendor', {
    url: '/kontakvendor',
    views: {
      'side-menu21': {
        templateUrl: 'templates/kontakVendor.html',
        controller: 'kontakVendorCtrl'
      }
    }
  })

  .state('menu.historyTransaksi', {
    url: '/historytransaksi',
    views: {
      'side-menu21': {
        templateUrl: 'templates/historyTransaksi.html',
        controller: 'historyTransaksiCtrl'
      }
    }
  })

  .state('menu.detailTransaksi', {
    url: '/detailtransaksiproyektor',
    views: {
      'side-menu21': {
        templateUrl: 'templates/detailTransaksi.html',
        controller: 'detailTransaksiCtrl'
      }
    }
  })

  .state('menu.profil', {
    url: '/profil',
    views: {
      'side-menu21': {
        templateUrl: 'templates/profil.html',
        controller: 'profilCtrl'
      }
    }
  })

  .state('menu.deposit', {
    url: '/deposit',
    views: {
      'side-menu21': {
        templateUrl: 'templates/deposit.html',
        controller: 'depositCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

$urlRouterProvider.otherwise('/side-menu21/home')

  

});