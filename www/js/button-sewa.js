// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function actionSewa() {
	
  // Create the search box and link it to the UI element.
  var barangsewa = document.getElementById('barang-sewa');
  var ambilantar = document.getElementById('ambil-antar');
  var tanggalsewa = document.getElementById('tanggal-sewa');
  var waktumulaisewa = document.getElementById('waktu-mulai-sewa');
  var jumlah = document.getElementById('jumlah');
  var keterangan = document.getElementById('keterangan');
  
  console.log("barang sewa : " + barangsewa +" ambil/antar : "+ambilantar+" tanggalsewa : "+tanggalsewa+" waktumulaisewa : "+waktumulaisewa+" jumlah : "+jumlah+" keterangan : "+keterangan);
};